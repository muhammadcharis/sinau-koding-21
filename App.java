import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        
        //cara ambil data dari luar
        Scanner scanner = new Scanner(System.in);
        String menu = "0";

        while (!menu.contains("4")){
            System.out.println("Hello, Guys!");
            System.out.println("Menu");
            System.out.println("1. Hitung luas persegi");
            System.out.println("2. Hitung luas segitiga");
            System.out.println("3. Hitung luas persegi panjang");
            System.out.println("4. Exit");
            menu = scanner.nextLine();
    
    
            if(menu.toLowerCase().contains("1")) luasPersegi(scanner);
            else if(menu.toLowerCase().contains("2")) luasSegitiga(scanner);
            else if(menu.toLowerCase().contains("3")) luasPersegiPanjang(scanner);
        }
    }

    private static void luasPersegi(Scanner scanner)
    {
        System.out.println("masukan sisi: ");
        int side = scanner.nextInt();
        int result = side*side;
        System.out.println("total luasnya adalah "+ result);
    }

    private static void luasPersegiPanjang(Scanner scanner)
    {
        System.out.println("masukan panjang: ");
        int length = scanner.nextInt();

        System.out.println("masukan lebar: ");
        int wide = scanner.nextInt();

        int result = length*wide;
        System.out.println("total luasnya adalah "+ result);
    }

    private static void luasSegitiga(Scanner scanner)
    {
        System.out.println("masukan alas: ");
        int base = scanner.nextInt();

        System.out.println("masukan tinggi: ");
        int high = scanner.nextInt();


        double result = 0.5 * (base*high);
        
        System.out.println("total luasnya adalah "+ (int) result);
    }
}
