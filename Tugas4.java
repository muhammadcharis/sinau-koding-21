import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Scanner;

public class Tugas4 {
    public static void main(String[] args) {
        
        
        Scanner scanner = new Scanner(System.in);
        try {
           
            System.out.println("Masukan jumlah siswa: ");
          
            int totalStudent = Integer.parseInt(scanner.nextLine());
            
            ArrayList<HashMap<String,String>> students = new ArrayList<HashMap<String,String>>();

            for (int i = 0; i < totalStudent; i++) {
                HashMap<String,String> student = new HashMap<String,String>();
                System.out.println("Mahasiswa ke : "+(i+1));
                System.out.println("Nama: ");
                String name = scanner.nextLine();
                System.out.println("Nilai: ");
                String value = scanner.nextLine();

                student.put("name", name);
                student.put("nilai", value);
                students.add(student);
            }

            System.out.println("Daftar Nilai Siswa");
            System.out.println("==============================================");
            Formatter fmt = new Formatter();

            fmt.format("%5s %15s %15s %15s\n","No", "Name", "Nilai", "Status");

            Integer no = 1;
            for (HashMap<String,String> row : students) {
                String finalResult;
               
                if(Integer.parseInt(row.get("nilai")) >= 65) finalResult = "lulus";
                else finalResult = "tidak lulus";

              
                fmt.format("%3s %14s %14s %17s\n", no,row.get("name"), row.get("nilai"), finalResult);  
                no++;
                
            }

            System.out.println(fmt);  

        } finally {
            scanner.close();
        }

            

            

        
        


        
    }
}
